import java.util.*;
public class OX {
     
    public static void main(String[] args){
        
        char [][] table = {{'-','-','-'},
                           {'-','-','-'},
                           {'-','-','-'}};
        char player = 'X';
        
        showWelcome();
        while(true){
            showTable(table);
            showTurn(player);
            inputRowcol(table,player);
            if(checkWin(table)){
                break;
            }else if(checkDraw(table)){
                break;
            }
            player = switchPlayer(player);
        }
        showTable(table);
       showWin(player,table);
       showBaBye(); } 
    
    public static void showWelcome(){
        System.out.println("Welcome to OX Game !");
    }
    public static void showTable(char [][] table){
        for(int a = 0; a < 3;a++){
               for(int j = 0; j < 3;j++){
                   System.out.print(" "+table[a][j]);
               }
               System.out.println();
           }
    }
    public static void showBaBye(){
        System.out.println("Ba Bye!");
    }
    public static void showTurn(char player){
        if(player == ('X')){
            System.out.println("X turn");
        }else{
            System.out.println("O turn");
        }
    }
    public static void inputRowcol(char [][] table,char player){
        try{
         Scanner scan = new Scanner(System.in);
         System.out.print("Please input Row and Col : ");
         int row = scan.nextInt();
         int col = scan.nextInt();
         inputNumber(row,col,table,player);
        }catch (Exception x){
            System.out.println("Please input again!");
            inputRowcol(table,player);
        }
         
    }
    
    public static void inputNumber(int row, int col,char [][] table,char player){
        if(row >= 1 && row <= 3 && col >= 1 && col <= 3){
           if(table [row - 1][col - 1] == '-' ){
               table [row - 1][col - 1] = player;
           }else{
               System.out.println("Please input one more time.");
               inputRowcol(table,player);
           }
          }else{
            System.out.println("You can't place here.");
              inputRowcol(table,player);
          }
    }
    
    public static void showWin(char player,char [][] table){
         if(checkWin(table)){
             System.out.println("Player "+player+" Win");
         }else{
             if(checkDraw(table)){
             System.out.println("Draw");
             }
         }
    }
    public static char switchPlayer(char player){
        if(player == ('O')){
            return 'X';
        }else{
            return 'O';
        }
    }
    public static boolean checkWin(char [][] table){
        for(int i = 0;i < 3;i++){
            if(checkRow(i,table)){
                return true;
            }else if(checkCol(i,table)){
                return true;
            }else if(checkOX(table)){
                return true;
            }else if(checkXO(table)){
                return true;
            }
        }
        return false;
    }
    public static boolean checkRow(int i,char [][] table){
        if(table[i][0] != ('-')){
            if(table[i][0] == (table[i][1]) && table[i][0] == table[i][2] ){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    public static boolean checkCol(int i,char [][] table){
        if(table[0][i] != ('-')){
            if(table[0][i] == (table[1][i])&& table[0][i] == (table[2][i])){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    public static boolean checkOX(char [][] table){
        if(table[1][1] != ('-')){
            if(table[0][0] == (table[1][1]) && table[0][0] == (table[2][2])){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    public static boolean checkXO(char [][] table){
        if(table[1][1] != ('-')){
            if(table[0][2] == (table[1][1]) && table[0][2] == (table[2][0])){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    public static boolean checkDraw(char [][]table){
        for(int i = 0;i < 3;i++){
            for(int j = 0;j < 3;j++){
                if(table[i][j] == '-'){
                    return false;
                }
            }
        }
        return true;
    }
    
    
}